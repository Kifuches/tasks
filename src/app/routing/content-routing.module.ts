import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContentComponent} from "@app/content/content.component";

const routes: Routes = [
  {path:'', component:ContentComponent},
  {path:'tasks', loadChildren:'app/content/tasks/tasks.module#TasksModule'},
  // {path:'users', loadChildren:'app/content/users/users.module#UsersModule'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule {}
