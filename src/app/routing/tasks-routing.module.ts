import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TasksComponent} from '@app/content/tasks/tasks.component';
import {ChangeComponent} from '@app/content/tasks/change/change.component';
import {AddComponent} from '@app/content/tasks/add/add.component';

const routes: Routes = [
  {path:'', component:TasksComponent, children:[
      {path:'add', component:AddComponent},
      {path:'change/:id', component:ChangeComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule {}
