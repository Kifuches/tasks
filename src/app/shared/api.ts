export class APIConf {
  static readonly guid = '9ad98e61-28fd-48e8-80ca-ba912a16f76a';

  static readonly main     = `http://intravision-task.test01.intravision.ru/`;
  static readonly main_api = `${APIConf.main}api/`;
  static readonly main_odata = `${APIConf.main}odata/`;

  static readonly priorities = `Priorities`;
  static readonly statuses = `Statuses`;
  static readonly users = `Users`;
  static readonly tasks = `tasks`;

  static concatUrl(main:string, child:string):string {
    return `${main}${APIConf.guid}/${child}`
  }
}
