import {createFeatureSelector, createSelector} from '@ngrx/store';
import {filter} from 'lodash';
import {typeMapTasks} from '@app/shared/store/tasks/interface';

const tasksState = createFeatureSelector('tasks');

export const selectFeatureCurrentTask = (id:number|any) => createSelector(
  tasksState,
  (map:typeMapTasks | any) => {
    return map.get(parseInt(id, 10));
  }
);
