import {TaskModel} from '@app/content/tasks/task.model';

export type typeMapTasks = Map<number, TaskModel>;

export interface ITaskTags{
  id: number;
  name: string;
}

export interface ILifetimeItems{
  id: number;
  userName: string;
  lifetimeType: number;
  createdAt: string;
  comment: string;
  fieldName: string;
  oldFieldValue: string;
  newFieldValue: string;
}
