import {typeMapTasks} from '@app/shared/store/tasks/interface';
import {ACTIONS_TASKS, ActionsTask} from '@app/shared/store/tasks/actions';
import {TaskModel} from '@app/content/tasks/task.model';

const initialState : typeMapTasks = new Map<number, TaskModel>();

export function reducerTasks(state = initialState, action: ActionsTask){
  switch (action.type){
    case ACTIONS_TASKS.ADD_LIST:
      return new Map(action.payload);

    case ACTIONS_TASKS.ADD:
      //  action.payload : ITaskItem
      state.set(action.payload.profile_id, action.payload);
      return new Map(state);

    case ACTIONS_TASKS.REMOVE:
      //  action.payload - id task for remove
      state.delete(action.payload);
      return new Map(state);

    default:
      return new Map(state);
  }
}
