import {Action} from "@ngrx/store";
import {TaskModel} from '@app/content/tasks/task.model';


export namespace ACTIONS_TASKS {
  export const ADD_LIST = '[Tasks] Add list';
  export const ADD      = '[Tasks] Add';
  export const REMOVE   = '[Tasks] Remove';
}

export class AddListTasks implements Action {
  readonly type : string = ACTIONS_TASKS.ADD_LIST;
  constructor(readonly payload:Array<any>){}
}

export class AddTask implements Action {
  readonly type : string = ACTIONS_TASKS.ADD;
  constructor(readonly payload:TaskModel){}
}

export class RemoveTask implements Action {
  readonly type : string = ACTIONS_TASKS.REMOVE;
  constructor(readonly payload:number){}
}

export type ActionsTask =
  AddListTasks  |
  AddTask       |
  RemoveTask    ;

