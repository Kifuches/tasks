import {Component, Input} from '@angular/core';
import {ILifetimeItems} from '@app/shared/store/tasks/interface';

@Component({
  selector: 'app-comment',
  template: `
    <div class="title">
      <div class="title__icon"></div>
      <div class="title__info">
        <p>{{comment.userName}}</p>
        <p class="date">{{comment.createdAt | sharedDatePipe:'DD MMM, hh:mm прокомментировал'}}</p>
      </div>
    </div>

    <div class="comment">
      <p>{{comment.comment}}</p>
    </div>
  `
})
export class CommentComponent {

  @Input() comment:ILifetimeItems;

  constructor(){}
}
