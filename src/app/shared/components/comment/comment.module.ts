import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CommentComponent} from '@app/shared/components/comment/comment.component';
import {SharedDatePipeModule} from '@app/shared/pipes/date.pipe/shared-date.module';

@NgModule({
  declarations: [CommentComponent],
  imports: [
    CommonModule,
    SharedDatePipeModule
  ],
  exports:[CommentComponent]
})
export class CommentModule {}
