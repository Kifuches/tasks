import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableHeaderComponent} from "@app/shared/components/table-header/table-header.component";

@NgModule({
  declarations: [TableHeaderComponent],
  imports: [
    CommonModule
  ],
  exports:[TableHeaderComponent]
})
export class TableHeaderModule {}
