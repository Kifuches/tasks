import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-table-header',
  template: `
    <tr class="table__header">
      <td *ngFor="let item of list">{{item}}</td>
    </tr>
  `
})
export class TableHeaderComponent implements OnInit {

  @Input() list = [];

  constructor() {}

  ngOnInit() {
  }

}
