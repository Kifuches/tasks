import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FieldInputComponent} from '@app/shared/components/field-input/field-input.component';

@NgModule({
  declarations: [FieldInputComponent],
  imports: [
    CommonModule
  ],
  exports: [FieldInputComponent]
})
export class FieldInputModule { }
