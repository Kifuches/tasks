import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-field-input',
  template: `
    <p>title</p>
    <input class="group__name"
           [value]="value"
           (change)="changeValue()">
  `
})
export class FieldInputComponent implements OnInit {

  @Input() value;
  @Input() title;
  @Output() onValue = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

  changeValue(){
    this.onValue.emit(this.value);
  }
}
