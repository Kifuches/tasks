import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'mapToArray'
})
export class MapToArrayPipe implements PipeTransform {

  transform(value: Map<any,any>, args?: any): any {
    try {
      return Array.from(value.values())
    }
    catch (e){
      console.error(e);
      return [];
    }
  }
}
