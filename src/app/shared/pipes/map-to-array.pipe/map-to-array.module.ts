import { NgModule } from '@angular/core';
import {MapToArrayPipe} from "@app/shared/pipes/map-to-array.pipe/map-to-array.pipe";


@NgModule({
  declarations: [
    MapToArrayPipe,
  ],
  imports: [],
  exports: [
    MapToArrayPipe
  ],
  providers: []
})
export class MapToArrayModule {}
