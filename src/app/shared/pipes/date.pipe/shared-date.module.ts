import {NgModule} from '@angular/core';
import {SharedDatePipe} from '@app/shared/pipes/date.pipe/shared-date.pipe';


@NgModule({
  declarations: [SharedDatePipe],
  exports: [SharedDatePipe]
})
export class SharedDatePipeModule {}
