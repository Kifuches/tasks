import {Pipe, PipeTransform} from "@angular/core";
import * as moment from 'moment';

@Pipe({
  name:"sharedDatePipe"
})
export class SharedDatePipe implements PipeTransform{
  transform(value:string, key:string): string {
    moment.locale('ru');
    let str = '';

    if(!key)str = moment(value).format('DD.MM.YYYY');
    else str = moment(value).format(key);

    return str == 'Invalid date' ? '' : str;
  }
}
