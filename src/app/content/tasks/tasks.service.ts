import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {APIContent} from "@app/content/api.content";
import {typeMapTasks} from '@app/shared/store/tasks/interface';
import {Store} from '@ngrx/store';
import {AddListTasks} from '@app/shared/store/tasks/actions';
import {Router} from '@angular/router';
import {TaskModel} from '@app/content/tasks/task.model';

@Injectable()
export class TasksService {

  api:APIContent;
  mapPriorities: Map<number, IPrioritiesItem>;
  mapStatuses: Map<number, IPrioritiesItem>;
  mapTasks: Map<number, TaskModel>;

  constructor(
    http:HttpClient,
    private router:Router,
    private store:Store<{tasks:typeMapTasks}>
  ) {
    this.api = new APIContent(http);
    this.mapPriorities = new Map();
    this.mapStatuses = new Map();
    this.mapTasks = new Map();
  }

  init(){
    this.getPriorities();
    this.getStatuses();
    this.getTasks();
  }

  private getPriorities(){
    this.api.get_priorities().subscribe((l:Array<IPrioritiesItem>) => {
      l.forEach(i => this.mapPriorities.set(i.id, i));
    });
  }

  private getStatuses(){
    this.api.get_statuses().subscribe((l:Array<IPrioritiesItem>) => {
      l.forEach(i => this.mapStatuses.set(i.id, i));
    });
  }

  private async getTasks(){
    await this.api.get_tasks().subscribe((l:IReqTasks) => {
      l.value.forEach(i => this.mapTasks.set(i.id, i));
      const tasks:Array<any> = l.value.map(i=>[i.id, i]);
      this.store.dispatch(new AddListTasks(tasks));
    });
  }
}


export interface IPrioritiesItem {
  id: number;
  name: string;
  rgb: string;
}

export interface IReqTasks {
  value: Array<TaskModel>;
  nextPageLink: string;
  count: number;
}
