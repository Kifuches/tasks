import {Component, OnInit} from '@angular/core';
import {TasksService} from '@app/content/tasks/tasks.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html'
})
export class TasksComponent implements OnInit {

  headerTable = ['', 'ID', 'Название', 'Статус', 'Исполнитель'];

  constructor(
    private ts: TasksService
  ) {}

  ngOnInit() {
    this.ts.init();
  }

  getRgb(priorityId:number){
    return this.ts.mapPriorities.get(priorityId).rgb;
  }
}
