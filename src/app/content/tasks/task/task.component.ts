import {Component, Input, OnInit} from '@angular/core';
import {TasksService} from "@app/content/tasks/tasks.service";
import {TaskModel} from '@app/content/tasks/task.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html'
})
export class TaskComponent implements OnInit {

  @Input() task:TaskModel;
  colorPriorities = '';

  constructor(
    public ts:TasksService
  ) {}

  ngOnInit() {
    this.colorPriorities = this.ts.mapPriorities.get(this.task.priorityId).rgb;
  }

}
