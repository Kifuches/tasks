import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TasksService} from '@app/content/tasks/tasks.service';
import {selectFeatureCurrentTask} from '@app/shared/store/tasks/selectors';
import {select, Store} from '@ngrx/store';
import {typeMapTasks} from '@app/shared/store/tasks/interface';
import {HttpClient} from '@angular/common/http';
import {TaskModel} from '@app/content/tasks/task.model';

@Component({
  selector: 'app-change-task',
  templateUrl: './change.component.html'
})
export class ChangeComponent implements OnInit, OnDestroy {

  task: TaskModel;

  activateRoute;

  constructor(
    private route:ActivatedRoute,
    private ts:TasksService,
    private http:HttpClient,
    private store:Store<{tasks:typeMapTasks}>
  ){
    this.task = new TaskModel(http);
  }

  ngOnInit() {
    this.activateRoute = this.route.paramMap.subscribe( (params) => {
      this.init(parseInt(params.get('id'), 10));
    });
  }

  init(id:number){
    this.store.pipe(select(selectFeatureCurrentTask(id))).subscribe((d:TaskModel) => {
      this.task
        .update(d)
        .getInfo();
    });
  }

  ngOnDestroy(): void{
    this.activateRoute ? this.activateRoute.unsubscribe() : null;
  }
}
