import {ILifetimeItems, ITaskTags} from '@app/shared/store/tasks/interface';
import {HttpClient} from '@angular/common/http';
import {APIConf} from '@app/shared/api';

export class TaskModel {
  id: number = null;
  name: string = '';
  description: string = '';
  createdAt: string;
  updatedAt: string;
  price: number;
  taskTypeId: number;
  taskTypeName: string;
  statusId: number;
  statusName: string;
  statusRgb: string;
  priorityId: number;
  priorityName: string;
  serviceId: number;
  serviceName: string;
  resolutionDatePlan: string;
  tags: Array<ITaskTags>;
  initiatorId: number;
  initiatorName: string;
  executorId: number;
  executorName: string;
  executorGroupId: number;
  executorGroupName: string;
  lifetimeItems: Array<ILifetimeItems>;

  constructor(
    private http:HttpClient
  ){
  }

  update(t:TaskModel){
    if(!t)return this;

    for(let i in t)
      this[i] = t[i];

    return this;
  }

  getInfo(){
    this.get_task(this.id).subscribe((t:TaskModel) => {
      this.lifetimeItems = t.lifetimeItems;
    });
  }

  private get_task(id){
    return this.http.get(APIConf.concatUrl(APIConf.main_api, `Tasks/${id}`));
  }
}
