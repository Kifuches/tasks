import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-task',
  templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {

  group:FormGroup;

  constructor(
    private fb:FormBuilder
  ) {
    this.group = fb.group({
      name: new FormControl('name', Validators.required),
      description: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  changeValue(event){
    console.log(event);
  }
}
