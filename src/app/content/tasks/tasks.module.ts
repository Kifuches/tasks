import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {TasksComponent} from "@app/content/tasks/tasks.component";
import {TasksRoutingModule} from "@app/routing/tasks-routing.module";
import {TasksService} from "@app/content/tasks/tasks.service";
import {MapToArrayModule} from "@app/shared/pipes/map-to-array.pipe/map-to-array.module";
import {CommonModule} from "@angular/common";
import {TableHeaderModule} from '@app/shared/components/table-header/table-header.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FieldInputModule} from '@app/shared/components/field-input/field-input.module';
import { AddComponent } from './add/add.component';
import {SharedDatePipeModule} from '@app/shared/pipes/date.pipe/shared-date.module';
import {TaskComponent} from '@app/content/tasks/task/task.component';
import {ChangeComponent} from '@app/content/tasks/change/change.component';
import {CommentModule} from '@app/shared/components/comment/comment.module';

@NgModule({
  declarations: [
    TasksComponent,
    TaskComponent,
    ChangeComponent,
    AddComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    TasksRoutingModule,

    MapToArrayModule,
    SharedDatePipeModule,

    TableHeaderModule,
    FieldInputModule,
    CommentModule
  ],
  providers: [
    TasksService
  ]
})
export class TasksModule {}
