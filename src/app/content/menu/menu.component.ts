import {Component, OnInit} from '@angular/core';
import {menu_list} from "@app/content/menu/menu.static";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  menu_list = menu_list;

  constructor() { }

  ngOnInit() {
  }

}
