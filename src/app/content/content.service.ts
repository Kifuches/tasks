import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {APIContent} from "@app/content/api.content";

@Injectable()
export class ContentService {

  api:APIContent;

  constructor(
    http:HttpClient
  ) {
    this.api = new APIContent(http)
  }
}
