import {Injectable} from "@angular/core";
import {APIConf} from "@app/shared/api";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class APIContent {

  constructor(
    private httpClient:HttpClient
  ){}

  get_priorities():Observable<any>{
    return this.httpClient.get(APIConf.concatUrl(APIConf.main_api, APIConf.priorities))
  }

  get_statuses():Observable<any>{
    return this.httpClient.get(APIConf.concatUrl(APIConf.main_api, APIConf.statuses))
  }

  get_tasks():Observable<any>{
    return this.httpClient.get(`${APIConf.main_odata}${APIConf.tasks}?tenantguid=${APIConf.guid}`)
  }
}
