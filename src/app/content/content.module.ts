import {NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {ContentRoutingModule} from "@app/routing/content-routing.module";
import {ContentComponent} from "@app/content/content.component";
import {APIContent} from "@app/content/api.content";

@NgModule({
  declarations:[
    ContentComponent
  ],
  imports:[
    HttpClientModule,
    ContentRoutingModule
  ],
  providers:[
    APIContent
  ]
})
export class ContentModule {}
