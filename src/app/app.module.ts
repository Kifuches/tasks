import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from "@app/app.component";
import {AppRoutingModule} from "@app/routing/app-routing.module";
import {HeaderComponent} from "@app/content/header/header.component";
import {MenuComponent} from "@app/content/menu/menu.component";
import {StoreModule} from '@ngrx/store';
import {reducerTasks} from '@app/shared/store/tasks/reduser';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({
      tasks: reducerTasks
    }),
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
