'use strict';

let gulp = require('gulp'),
  sass = require('gulp-sass'),
  concat = require('gulp-concat'),
  cleanCSS = require('gulp-clean-css');

gulp.task('sass', ()=> {
  return gulp.src([
    './sass/*.sass',
    './sass/**/*.scss'
  ])
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(cleanCSS())
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('./src'));
});
